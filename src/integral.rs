use rand::prelude::*;

#[derive(Copy,Clone,Debug)]
pub struct Limits {
    pub min_y: f64,
    pub max_y: f64,
    pub min_x: f64,
    pub max_x: f64
}

pub fn montecarlo_int<F>(data: Limits, ngen: usize, func: F) -> f64
    where F: Fn(f64) -> f64 {

    let mut rng = thread_rng();
    let mut n = 0;

    for _ in 0..ngen {
        let x: f64 = rng.gen_range(data.min_x, data.max_x);
        let y: f64 = rng.gen_range(data.min_y, data.max_y);

        if y < func(x) {
            n += 1;
        }
    }

    let mul = (data.max_y - data.min_y) * (data.max_x - data.min_x);
    mul * n as f64 / ngen as f64
}


