// Extern crates
extern crate rand;

// Our modules
pub mod pi;
pub mod integral;

// Re-exports
pub mod prelude {
    pub use pi::montecarlo_pi;
    pub use pi::calculate_pi;
    pub use integral::Limits;
    pub use integral::montecarlo_int;
}

#[cfg(test)]
mod test {

    #[test]
    fn monte_int() {
        use integral::Limits;
        use integral::montecarlo_int;

        let f = | x: f64 | x * x;
        let int = montecarlo_int(Limits { min_y: 0.0, max_y: 1.0, min_x: 0.0, max_x: 1.0 }, 80000, f);
        println!("monte_int: {}", int);
    }

    #[test]
    fn problem_a() {
        use integral::Limits;
        use integral::montecarlo_int;

        let f = | x: f64 | (-x.powf(2.0)).exp();
        let int = montecarlo_int(Limits { min_y: 0.0, max_y: 1.0, min_x: 0.0, max_x: 1.0 }, 80000, f);
        println!("problem_a: {}", int);
    }

    #[test]
    fn problem_b() {
        use integral::Limits;
        use integral::montecarlo_int;

        let f = | x: f64 | x;
        let int = 2.0 * montecarlo_int(Limits { min_y: 0.0, max_y: 1.0, min_x: 0.0, max_x: 1.0 }, 80000, f);
        println!("problem_b: {}", int);
    }

    #[test]
    fn problem_c() {
        use integral::Limits;
        use integral::montecarlo_int;

        let f = | x: f64 | x * x;
        let int = 4.0 * montecarlo_int(Limits { min_y: 0.0, max_y: 1.0, min_x: 0.0, max_x: 1.0 }, 80000, f);
        println!("problem_c: {}", int);
    }
}
