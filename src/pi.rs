use rand::prelude::*;

pub fn montecarlo_pi(ngen: usize) -> f64 {
  let mut rng = thread_rng();
  let mut n = 0;

  for _ in 0..ngen {
    let x: f64 = rng.gen_range(-1.0, 1.0);
    let y: f64 = rng.gen_range(-1.0, 1.0);
    let r: f64 = (x.powi(2) + y.powi(2)).sqrt();
    if r < 1.0 { n += 1; }
  }

  4.0 * n as f64/ ngen as f64
}

pub fn calculate_pi(ngen: usize, times: usize) -> f64 {
  let mut sum = 0.0;
  for _ in 0..times {
    sum += montecarlo_pi(ngen);
  }

  sum / (times as f64)
}

